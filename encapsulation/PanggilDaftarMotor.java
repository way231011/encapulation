package encapsulation;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class PanggilDaftarMotor {
    public static void main(String[] args) {

        DaftarMotor daftarMotor = new DaftarMotor();
        daftarMotor.setNama("ZX10");
        daftarMotor.setHarga(1500000000);
        daftarMotor.setCc(1400);
        daftarMotor.setMerk("KAWASAKI");
        daftarMotor.setTahun(2019);

        DecimalFormat kursIdn = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIdn.setDecimalFormatSymbols(formatRp);
        String a = kursIdn.format(daftarMotor.getHarga());
        System.out.println("=====DAFTAR MOTOR=====");
        System.out.println("Nama = " + daftarMotor.getNama());
        System.out.println("Harga = " + a);
        System.out.println("CC = " + daftarMotor.getCc() + "cc");
        System.out.println("Merk = " + daftarMotor.getMerk());
        System.out.println("Tahun = " + daftarMotor.getTahun());

        ListMotor listMotor = new ListMotor();
        listMotor.setNama(daftarMotor.getNama());
        listMotor.setHarga(daftarMotor.getHarga());
        listMotor.setMerk(daftarMotor.getMerk());

        System.out.println();

        System.out.println("=====LIST MOTOR=====");
        System.out.println("Nama = " + listMotor.getNama());
        System.out.println("Harga = " + a);
        System.out.println("Merk = " + listMotor.getMerk());
    }
}
