package encapsulation;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class PanggilMenuMakanan {
    public static void main(String[] args) {
        MenuMakanan menuMakanan = new MenuMakanan();
        menuMakanan.setMenuMakanan("Ayam Rica");
        menuMakanan.setMenuMinuman("Aqua");
        menuMakanan.setTotalHarga(30000);
        DecimalFormat kursIdn = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIdn.setDecimalFormatSymbols(formatRp);
        System.out.println(menuMakanan.getMenuMakanan());
        System.out.println(menuMakanan.getMenuMinuman());
        System.out.println(kursIdn.format(menuMakanan.getTotalHarga()));
    }
}
