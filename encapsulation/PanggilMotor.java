package encapsulation;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class PanggilMotor {
    public static void main(String[] args) {
        Motor motor = new Motor();
        motor.setMerkMotor("Kawasaki");
        motor.setJenisMotor("H2");
        motor.setHarga(10000000000L);
        DecimalFormat kursIdn = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIdn.setDecimalFormatSymbols(formatRp);
        System.out.println(motor.getMerkMotor());
        System.out.println(motor.getJenisMotor());
        System.out.println(kursIdn.format(motor.getHarga()));
    }
}
