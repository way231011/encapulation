package encapsulation;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class PanggilMobil {
    public static void main(String[] args) {

        methodMobil("BMW","VOLVO",1000000000L);


    }

    public static Mobil methodMobil(String merkMobil,String jenismobil,Long harga){
        Mobil mobil = new Mobil();
        mobil.setMerkMobil(merkMobil);
        mobil.setJenisMObil(jenismobil);
        mobil.setHargaMobil(harga);

        DecimalFormat kursIdn = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIdn.setDecimalFormatSymbols(formatRp);
        System.out.println(mobil.getMerkMobil());
        System.out.println(mobil.getJenisMObil());
        System.out.println(kursIdn.format(mobil.getHargaMobil()));
        return mobil;
    }
}
