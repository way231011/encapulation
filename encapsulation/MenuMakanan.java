package encapsulation;

public class MenuMakanan {
    private String menuMakanan;
    private String menuMinuman;
    int totalHarga;

    public String getMenuMakanan() {
        return menuMakanan;
    }

    public void setMenuMakanan(String menuMakanan) {
        this.menuMakanan = menuMakanan;
    }

    public String getMenuMinuman() {
        return menuMinuman;
    }

    public void setMenuMinuman(String menuMinuman) {
        this.menuMinuman = menuMinuman;
    }

    public int getTotalHarga() {
        return totalHarga;
    }

    public void setTotalHarga(int totalHarga) {
        this.totalHarga = totalHarga;
    }
}

