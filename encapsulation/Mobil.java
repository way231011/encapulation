package encapsulation;

public class Mobil {
    private String merkMobil;
    private String jenisMObil;
    private Long hargaMobil;

    public String getMerkMobil() {
        return merkMobil;
    }

    public void setMerkMobil(String merkMobil) {
        this.merkMobil = merkMobil;
    }

    public String getJenisMObil() {
        return jenisMObil;
    }

    public void setJenisMObil(String jenisMObil) {
        this.jenisMObil = jenisMObil;
    }

    public Long getHargaMobil() {
        return hargaMobil;
    }

    public void setHargaMobil(Long hargaMobil) {
        this.hargaMobil = hargaMobil;
    }
}
